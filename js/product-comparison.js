(function(){

    var pct = angular.module('productComparison', ['pascalprecht.translate']);

    /**
        Main controller
    **/
    pct.controller('ComparisonCtrl', function($scope, $http) {

        $scope.products = [];
        $scope.table = { details: null };
        $scope.selectedProducts = [ { details: null },  { details: null },  { details: null }];
        $scope.selected = 0;

        //get products from json file
        $http.get('products.json').success(function(data) {
            $scope.products = data;
        })
        .error(function (data, status, headers, config) {
            //  Do some error handling here
        })

        //show selected product on table or show next product if 'Select' was selected
        $scope.selectProduct = function(product, p) {

            $scope.selectedProducts[p].details = product;

            if($scope.selectedProducts[p].details === null && $scope.checkUnselectedProducts() < 3)
                $scope.navigateProduct(1, true);
            else 
                $scope.selected = p;
        };

        //product navigation for mobile
        $scope.navigateProduct = function(step, deselected) {

            //skip if not enough products selected to navigate through
            if(!deselected && !$scope.navigatable())
                return false;

            var sel = $scope.selected + step;

            if(sel >= 3) sel = 0;
            else if(sel <= -1) sel = 2;

            $scope.selected = sel;

            //navigate to next element if current dropdown is not selected
            if($scope.selectedProducts[sel].details === null)
                $scope.navigateProduct(step, true);

        };

        $scope.navigatable = function() {

            return $scope.checkUnselectedProducts() < 2;

        };

        //get amount of unselected dropdowns
        $scope.checkUnselectedProducts = function() {

            var nulls = 0;

            for(i = 0; i < $scope.selectedProducts.length; i++) {
                if($scope.selectedProducts[i].details === null)
                    nulls++;
            }

            return nulls;

        };

    });

    /**
        Filter for preventing selecting same products on the select boxes
    **/
    pct.filter('noMultiple', function() {
        return function(product, scope, p) {

            var sel = scope.selectedProducts;
            var output = [];

            for(j = 0; j < product.length; j++) {
            
                var selected = false;

                for(i = 0; i < sel.length; i++) {

                    //check if product is already selected in another(!) select box
                    if(p != i && angular.equals(sel[i].details, product[j])) {
                        selected = true;
                        break;
                    }                

                }

                if(!selected) 
                    output.push(product[j]);

            }

            return output;
        };
    });

    /**
        Directive for displaing product details
    **/
    pct.directive('productDetails', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'directives/product-details.html',
            scope: {
                productDetails: '=details'
            }
        }
    });

    /**
        i18n Support
    **/
    pct.config(function ($translateProvider) {

        //English
        $translateProvider.translations('en_UK', {
            'APP_HEADLINE': 'Compare Products',
            'SELECT': 'Select',
            'NO_RESULTS': 'Please select a product from the dropdowns above.'
        });

        //Japanese
        $translateProvider.translations('jp_JP', {
            'APP_HEADLINE': '製品比較',
            'SELECT': '選択してください',
            'NO_RESULTS': '上のボタンから製品を選択してください。'
        });

        $translateProvider.preferredLanguage('en_UK');

    });

})();